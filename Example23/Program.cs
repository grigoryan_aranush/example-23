﻿using System;
class Example
{

    static void Main()
    {

        int x, y, i;

        x = 1;
        y = 0;
        Console.WriteLine("Series using y=y +x++");
        for (i = 0; i < 10; i++)
        {
            y = y + x++;
            Console.WriteLine(y + " ");
        }
        Console.WriteLine();
        x = 1;
        y = 0;
        Console.WriteLine("Series using y=y + ++x");
        for (i = 0; i < 10; i++)
        {

            y = y + ++x;
            Console.WriteLine(y + " ");

        }
    }
}